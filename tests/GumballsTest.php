<?php

namespace GumballsMachine;

require 'GumballsMachine.php';


class GumballsMachineTest extends \PHPUnit_Framework_TestCase
{
    public $GumballMachineInstance;

    public function setUp()
    {
        $this->GumballMachineInstance = new GumballsMachine();
    }

    public function testIfWheelWorks()
    {
        //Suppose we have 100 gumballs...
        $this->GumballMachineInstance->setGumballs(100);

        //And we turn the wheel once...
        $this->GumballMachineInstance->turnWheel();

        //we should now have 99 gumballs remaining in the machine right?
        $this->assertEquals(99, $this->GumballMachineInstance->getGumballs());

    }

    public function holatest()
    {
        $this->assertEquals("Hola mundo", $this->GumballMachineInstance->hola());
    }
}