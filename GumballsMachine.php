<?php

namespace GumballsMachine {

    class GumballsMachine
    {
        private $gumballs;


        //Get the amount of gumballs still in the machine
        public function getGumballs()
        {
            return $this->gumballs;
        }

        //Set the amount of gumballs in the machine
        public function setGumballs($amount)
        {
            $this->gumballs = $amount;
        }

        //The user turns the wheel, machine dispnses gumball
        public function turnWheel()
        {
            $this->setGumballs($this->getGumballs() - 1);
        }

        public function hello()
        {
            return "Hola mundo";
        }
    }
}
